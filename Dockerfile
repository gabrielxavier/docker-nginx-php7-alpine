FROM alpine:3.12
LABEL maintainer = "Gabriel Xavier | https://gabrielxavier.com"

ENV SITE_PATH=/var/www/html/
ENV TZ=America/Sao_Paulo

RUN set -ex; \
    appInstall=" \
        curl \
        git \
        nginx \
        postfix \
        supervisor \
        wget \
        tzdata \
        zip \
    "; \
    phpInstall=" \
        php7 \
        php7-apcu \
        php7-bcmath \
        php7-bz2 \
        php7-calendar \
        php7-cgi \
        php7-ctype \
        php7-curl \
        php7-dev \
        php7-dom \
        php7-fileinfo \
        php7-fpm \
        php7-gd \
        php7-gettext \
        php7-gmp \
        php7-iconv \
        php7-intl \
        php7-json \
        php7-mbstring \
        php7-mcrypt \
        php7-memcached \
        php7-mysqli \
        php7-odbc \
        php7-opcache \
        php7-openssl \
        php7-pear \
        php7-phar \
        php7-pdo \
        php7-pdo_dblib \
        php7-pdo_mysql \
        php7-pdo_odbc \
        php7-pdo_pgsql \
        php7-pdo_sqlite \
        php7-tokenizer \
        php7-redis \
        php7-session \
        php7-simplexml \
        php7-soap \
        php7-sqlite3 \
        php7-tidy \
        php7-xdebug \
        php7-xml \
        php7-xmlreader \
        php7-xmlrpc \
        php7-zip \
    "; \
    apk update && apk --no-cache add $appInstall $phpInstall; \
    \
    mkdir -p /var/log/supervisor; \
    mkdir -p ${SITE_PATH}; \
    cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo "${TZ}" > /etc/timezone; \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

WORKDIR ${SITE_PATH}

COPY ./webcode/ ${SITE_PATH}
COPY ./config/nginx.conf /etc/nginx/nginx.conf
COPY ./config/site.conf /etc/nginx/conf.d/default.conf
COPY ./config/www.conf /etc/php7/php-fpm.d/zzz_custom.conf
COPY ./config/php.ini /etc/php7/conf.d/zzz-setting.ini
COPY ./config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

#RUN /usr/bin/composer install
RUN chown -R nginx:nginx ${SITE_PATH} && \
    chmod -R 777 ${SITE_PATH}

EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
