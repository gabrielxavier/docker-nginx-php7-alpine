[![Build status](https://gitlab.com/gabrielxavier/docker-nginx-php7-alpine/badges/master/build.svg)](https://gitlab.com/gabrielxavier/docker-nginx-php7-alpine/commits/master)
### WebServer with Nginx, PHP, Supervisord and Composer on Alpine

#### Last versions of:

- Alpine 3.12 
- Nginx 1.18
- PHP 7.3
- Supervisord 4.2
- Composer 1.10

        Volume: /var/www/html
        Port: 80

[DockerHub](https://hub.docker.com/r/gabxav/webserver/)
